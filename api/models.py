from django.db import models


# OneToOne Relationship.....>>>>!
class ExtraInfo(models.Model):
    RODZAJE = {
        (0, 'Unknown'), (1, 'Horror'), (2, 'Drama'),
        (3, 'Comedy'), (4, 'Sci-fi'), (5, 'Thriller'),
    }
    czas_trwania = models.IntegerField()
    rodzaj = models.IntegerField(choices=RODZAJE, default=0)


class Film(models.Model):
    tytul = models.CharField(max_length=32)
    opis = models.TextField(max_length=256)
    po_premierze = models.BooleanField(default=False)
    premiera = models.DateField(null=True, blank=True)
    rok = models.IntegerField()
    imdb_rating = models.DecimalField(max_digits=4, decimal_places=2,
                                      null=True, blank=True)
    extra_info = models.OneToOneField(ExtraInfo, on_delete=models.CASCADE,
                                      null=True, blank=True)

    def __str__(self):
        return self.moja_nazwa()

    def moja_nazwa(self):
        return self.tytul + " (" + str(self.rok) + ")"


# ManyToOne relationship...>>>>>>>!
class Recenzja(models.Model):
    opis = models.TextField(default='')
    gwiazdki = models.IntegerField(default=5)
    film = models.ForeignKey(Film, on_delete=models.CASCADE,
                             related_name='recenzje')


# ManyToMany Relationship......>>>!>>>>!!
class Aktor(models.Model):
    imie = models.CharField(max_length=32)
    nazwisko = models.CharField(max_length=32)
    filmy = models.ManyToManyField(Film, related_name='aktorzy')

    def __str__(self):
        return self.imie + ' ' + self.nazwisko
