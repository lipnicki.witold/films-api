from django.urls import include, path
from rest_framework import routers
from api import views

router = routers.DefaultRouter()
router.register(r'users', views.UserViewSet)
router.register(r'filmy', views.FilmsViewSet)
router.register(r'recenzje', views.RecenzjeViewSet)
router.register(r'aktorzy', views.AktorViewSet)

urlpatterns = [
    path('', include(router.urls)),
]
