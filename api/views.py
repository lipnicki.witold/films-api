from django.contrib.auth.models import User
from django.http import HttpResponseNotAllowed
from rest_framework import viewsets, permissions
from rest_framework.decorators import action
from rest_framework.response import Response

from api.serializer import UserSerializer
from .models import Film, Recenzja, Aktor
from .serializer import FilmSerializer, FilmMiniSerializer, RecenzjaSerializer, AktorSerializer, FilmFullSerializer


class UserViewSet(viewsets.ModelViewSet):
    queryset = User.objects.all().order_by('-date_joined')
    serializer_class = UserSerializer
    #permission_classes = [permissions.IsAuthenticated]


class FilmsViewSet(viewsets.ModelViewSet):
    queryset = Film.objects.filter(po_premierze=True) #all()
    serializer_class = FilmFullSerializer
    #permission_classes = [permissions.IsAuthenticated]

    def get_queryset(self):
        #return Film.objects.filter(po_premierze=True)
        rok = self.request.query_params.get('rok', None)
        id = self.request.query_params.get('id', None)

        if id:
            film = Film.objects.filter(id=id)
            return film
        else:
            if rok:
                filmy = Film.objects.filter(rok=rok)
            else:
                filmy = Film.objects.all()
        return filmy

    def list(self, request, *args, **kwargs):
        queryset = self.get_queryset()
        #serializer = FilmMiniSerializer(queryset, many=True)
        serializer = FilmFullSerializer(queryset, many=True)
        return Response(serializer.data)

    def retrieve(self, request, *args, **kwargs):
        instance = self.get_object()
        serializer = FilmFullSerializer(instance)
        return Response(serializer.data)

    def create(self, request, *args, **kwargs):
        if request.user.is_staff:
            film = Film.objects.create(tytul=request.data['tytul'],
                                   opis=request.data['opis'],
                                   po_premierze=request.data['po_premierze'],
                                   rok=request.data['rok'])
            serializer = FilmSerializer(film, many=False)
            return Response(serializer.data)
        else:
            return HttpResponseNotAllowed('')

    def update(self, request, *args, **kwargs):
        instance = self.get_object()
        instance.tytul = request.data['tytul']
        instance.opis = request.data['opis']
        instance.po_premierze = request.data['po_premierze']
        instance.rok = request.data['rok']
        instance.save()
        serializer = FilmSerializer(instance, many=False)
        return Response(serializer.data)

    def destroy(self, request, *args, **kwargs):
        instance = self.get_object()
        instance.delete()
        return Response('Film usuniety')

    @action(detail=True)
    def premiera(self, request, **kwargs):
        instance = self.get_object()
        instance.po_premierze = True
        instance.save()
        serializer = FilmSerializer(instance, many=False)
        return Response(serializer.data)

    @action(detail=False)
    def premiera_wszystkie(self, request, **kwargs):
        instance = Film.objects.all()
        instance.update(po_premierze=True)
        serializer = FilmSerializer(instance, many=True)
        return Response(serializer.data)

    @action(detail=False, methods='post')
    def przed_premiera_wszystkie(self, request, **kwargs):
        instance = Film.objects.all()
        instance.update(po_premierze=request.data['po_premierze'])
        serializer = FilmSerializer(instance, many=True)
        return Response(serializer.data)


class RecenzjeViewSet(viewsets.ModelViewSet):
    queryset = Recenzja.objects.all()
    serializer_class = RecenzjaSerializer


class AktorViewSet(viewsets.ModelViewSet):
    queryset = Aktor.objects.all()# sv wef
    serializer_class = AktorSerializer

    @action(detail=True, methods=['post'])
    def dolacz(self, request, **kwargs):
        aktor = self.get_object()
        film = Film.objects.get(id=request.data['film'])
        aktor.filmy.add(film) # mozna zrobic nowa metode i add zami x cenic na remove

        serializer = AktorSerializer(aktor, many=False)
        return Response(serializer.data)
