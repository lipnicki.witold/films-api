# Generated by Django 4.0.3 on 2022-04-11 08:03

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('api', '0003_extrainfo_film_extra_info'),
    ]

    operations = [
        migrations.AlterField(
            model_name='extrainfo',
            name='rodzaj',
            field=models.IntegerField(choices=[(5, 'Thriller'), (3, 'Comedy'), (2, 'Drama'), (4, 'Sci-fi'), (0, 'Unknown'), (1, 'Horror')], default=0),
        ),
        migrations.CreateModel(
            name='Recenzja',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('opis', models.TextField(default='')),
                ('gwiazdki', models.IntegerField(default=5)),
                ('film', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='api.film')),
            ],
        ),
    ]
